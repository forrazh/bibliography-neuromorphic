\documentclass[fontsize=10pt,twoside]{scrartcl}
\usepackage{ulille-rapport}
\usepackage[citestyle=numeric-comp]{biblatex}
\addbibresource{export-from-zotero.bib}


%%%% Titre, auteur, pied de page
%% Le logo en haut à gauche de la page est chargé par les commandes
%% \maketitle ou \logoULille
%% Il peut être adapté en redéfinissant la commande \includeLogo comme suit :
\renewcommand{\includeLogo}{\includegraphics[height=2cm]{FST-ULille-RVB-H-2021.pdf}}
\title{Bibliographical report of the Neuromorphic Computing course:\\
\large ETH Zurich / Giacomo Indiveri / Tobi Delbruck}
\author{Adrien Grzechowiak}
\date{\today}
\piedDePage{\color{GrisStatutaire} This document has been written as an assignment of the Neuromorphic Computing class
 of the Internet of Things master at the University of Lille.}

\begin{document}
%%%% Choix de la langue du document
%\selectlanguage{french}
\selectlanguage{english}

\maketitle

\section{Introduction}

Two significant figures from ETH Zurich, Giacomo Indiveri and Tobias 'Tobi' Delbruck, are featured in this bibliography.
 These personalities are crucial in the IT domain and for their significant contribution to neuroscience.
The development of artificial neural processing systems, which utilize engineering, IT, physics, and neuroscience, is 
becoming more understandable thanks to them.

Giacomo Indiveri is an engineer who has a strong interest in neurology, computer science, and physics. His research
 explores artificial neural processing systems by integrating these fields. His focus is on creating neuromorphic 
 cognitive systems using VLSI technology, which includes neural networks with multipurpose spiking neural architectures.
  As a first step toward a new generation of computing technologies, these real-time systems are intended to test 
  computational paradigms inspired by the brain in practical settings.

Tobias Delbruck is an American neuromorphic engineer and a titular professor of physics and electrical engineering at
 ETH Zurich. His areas of interest include event-based algorithms, sensor applications, spike-event-based silicon retina 
 vision sensors, and silicon cochlea auditory sensors. He is acknowledged as a Fellow of the IEEE and is an important 
 member, along with Shih-Chii Liu and Giacomo Indiveri, of the hardware group at INI. He also actively organizes the 
 Telluride Workshop on Neuromorphic Engineering.

This bibliography explores the remarkable achievements and contributions of Giacomo Indiveri and Tobias Delbruck in 
the fascinating field of neuromorphic and brain-inspired technology at ETH Zurich.

\section{Historical Subjects}
The first historical article we are going to discuss is \textit{"Orientation-selective aVLSI spiking neurons"}
\cite{liu_orientation-selective_2001}. This article is focused on a multi-chip spike-based system used to synthesize 
oriented-tuned neurons using two different models : feedforward and feedback.
They have used a system consisting of a silicon retina, a PIC microcontroller, and a transceiver chip whose
 integrate-and-fire neurons are connected in a soft winner-take-all architecture
The multi-chip VLSI system can support spike-based cortical processing models, and it's used to attenuate the simulation 
time because these hardware simulation systems have a real-time property and the size of the network does not affect the 
simulation time.
In this document, they also explain how they synthesized orientation-tuned spiking neurons using the multi-chip system and
 how their analog hardware spiking model matched their experimental observations.
Thanks to these observations, they will be testing faster boards that can handle the mapping of more neurons and extend
 the multi-chip VLSI system we discussed in this document to a more sophisticated system that supports multiple senders 
and multiple receivers.
As they've said, "the success of this system opens up the way for more elaborate spike-based emulations in the future".

The next paper is about a book named \textit{"Analog VLSI: Circuits and Principles"}\cite{liu_analog_2002}. From the 
publisher: "Neuromorphic engineers work to improve the performance of artificial systems through the development of chips 
and systems that process information collectively using primarily analog circuits. This book presents the central concepts required for the creative and successful design of analog VLSI circuits. The discussion is weighted toward novel circuits that emulate natural signal processing. Unlike most circuits in commercial or industrial applications, these circuits operate mainly in the subthreshold or weak inversion region. Moreover, their functionality is not limited to linear operations but also encompasses many interesting nonlinear operations similar to those occurring in natural systems. Topics include device physics, linear and nonlinear circuit forms, translinear circuits, photodetectors, floating-gate devices, noise analysis, and process technology."

\section{Main Results}

Now we are going to talk about some main results, and we will begin with \textit{"A VLSI array of low-power spiking neurons and bistable synapses with spike-timing dependent plasticity"}\cite{indiveri_vlsi_2006}. This document presents different things, such as a new learning mechanism called spike-timing dependent plasticity (STDP) to compute complex information processing tasks.
It also introduces a mixed-mode analog VLSI device with experimental data results indicating the possible use of massively parallel VLSI networks of I-F neurons to simulate real–time complex spike-based learning algorithms that use address-event representation (AER) to receive and transmit spikes.
This property allows us to implement learning mechanisms useful for real-time unsupervised learning tasks and is ideal for constructing compact VLSI sensory systems with adaptation and learning capabilities.

The second article, \textit{"Neuromorphic silicon neuron circuits"}\cite{indiveri_neuromorphic_2011} provides an overview of a variety of neuromorphic silicon neurons and provides implementation solutions for silicon neurons, detailing the most popular building blocks and approaches.
A wide range of constructed VLSI chips will be used to measure experimental outcomes, which will be used to compare the design approaches employed for each silicon neuron design that has been detailed.
Also, it discusses digital tools and simulators that are practical for exploring neural network quantitative behavior but are suboptimal for real-time systems or detailed large-scale neural simulations introducing Silicon neurons (SiNs) which are hybrid analog/digital VLSI circuits emulating real neurons' electrophysiological behavior. They offer efficient, real-time, large-scale neural emulation with varying complexity.
The term "neuromorphic" initially referred to analog VLSI circuits using the same physics as the nervous system. It now includes analog and digital hardware implementations and spike-based sensory processing systems.
This work covers various SiN design circuits, employing different strategies and techniques such as current-mode, sub-threshold, voltage-mode, and switched-capacitor designs. It provides an overview of recent silicon neuron circuit designs, comparing approaches and highlighting each design's advantages.


The third document is a book named \textit{"Event-Based Neuromorphic Systems"}\cite{liu_event-based_2014}. From the publisher : "Neuromorphic electronic engineering takes its inspiration from the functioning of nervous systems to build more power-efficient electronic sensors and processors. Event-based neuromorphic systems are inspired by the brain's efficient data-driven communication design, which is key to its quick responses and remarkable capabilities.  This cross-disciplinary text establishes how circuit building blocks are combined in architecture to construct complete systems. These include vision and auditory sensors, as well as neuronal processing and learning circuits that implement models of nervous systems.
Techniques for building multi-chip scalable systems are considered throughout the book, including methods for dealing with transistor mismatch, extensive discussions of communication and interfacing, and making systems that operate in the real world. The book also provides historical context that helps relate the architectures and circuits to each other and guides readers to the extensive literature. Chapters are written by founding experts and have been extensively edited for overall coherence.
This pioneering text is an indispensable resource for practicing neuromorphic electronic engineers, advanced electrical engineering and computer science students, and researchers interested in neuromorphic systems."


\section{Ongoing Research}


The first document related to ongoing research is \textit{"Embodied neuromorphic intelligence"}\cite{bartolozzi_embodied_2022}. Focused on robotics, this document aims to address current challenges in robotics and neuromorphic technology. It suggests research directions to overcome obstacles and facilitate the development of intelligent robotic systems driven by neuromorphic technology.
Neuromorphic engineering utilizes mixed-signal analog and digital hardware, enabling the implementation of neural 
computational primitives inspired by biological intelligence. This approach offers energy-efficient and compact solutions 
to support intelligence implementation on robotic platforms.
Efforts are made in this document to enhance performance and robustness by presenting initial attempts, identifying open 
challenges, and proposing actions needed to overcome current limitations.


The second article \textit{"Reconfigurable halide perovskite nanocrystal memristors for neuromorphic computing"}
\cite{john_reconfigurable_2022} discusses the challenges in achieving desired computational complexity in many in-memory 
computing frameworks due to the specific switching characteristics required in electronic devices. Existing memristive
devices are limited by their inability to be reconfigured for diverse switching requirements, relying on tailored material
 designs for specific applications. The text introduces a solution in the form of a reconfigurable halide perovskite 
 nanocrystal memristor. This device allows on-demand switching between diffusive (volatile) and drift (non-volatile) 
 modes through controllable electrochemical reactions. The chosen nanocrystals and organic capping ligands enable 
 excellent endurance performances in both volatile (2 × 106 cycles) and non-volatile (5.6 × 103 cycles) modes. The 
 relevance of these proof-of-concept perovskite devices is demonstrated in a benchmark reservoir network with volatile 
 recurrent and non-volatile readout layers, involving 19,900 measurements across 25 dynamically configured devices.

\section{Conclusion}

To conclude, the bibliography provides a comprehensive examination of the significant contributions made by Giacomo 
Indiveri and Tobias Delbruck from ETH Zurich to the field of neuromorphic and brain-inspired technology. Their work 
crosses the realms of engineering, information technology, physics, and neuroscience, paving the way for the development 
of artificial neural processing systems.

The historical subjects\cite{liu_orientation-selective_2001,liu_analog_2002} discussed in the bibliography highlight key
articles and a book that dig into the synthesis of oriented-tuned neurons using multi-chip spike-based systems, the 
principles of analog VLSI circuits, and their applications in neuromorphic engineering. These historical works form the 
foundation for understanding the evolution of neuromorphic technology.

The main results\cite{indiveri_vlsi_2006,indiveri_neuromorphic_2011,liu_event-based_2014} section introduce innovative 
learning mechanisms like spike-timing-dependent plasticity and explore the design and implementation of neuromorphic 
silicon neurons, showcasing the potential for real-time complex spike-based learning algorithms.

Moreover, the section on ongoing research\cite{bartolozzi_embodied_2022,john_reconfigurable_2022} highlights the 
challenges in robotics and propose solutions involving mixed-signal analog and digital hardware for intelligent robotic 
systems. Additionally, the exploration of reconfigurable halide perovskite nanocrystal memristors presents a promising 
avenue for overcoming limitations in achieving desired computational complexity in in-memory computing frameworks.

This bibliography provides a comprehensive overview of the evolution of neuromorphic technology, exploring historical 
perspectives, key contributions, and ongoing research, positioning Giacomo Indiveri and Tobias Delbruck as significant 
figures in shaping the landscape of this field at ETH Zurich.

\newpage
\printbibliography
\end{document}
